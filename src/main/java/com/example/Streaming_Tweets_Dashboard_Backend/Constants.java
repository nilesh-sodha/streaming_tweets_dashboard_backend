package com.example.streaming_tweets_dashboard_backend;

/**
 * @author nileshsodha
 */
public class Constants {
    public static final String KAFKA_PUBLISH_TWEET = "twitter.tweet";
    public static final String KAFKA_PUBLISH_TWEET_TEST = "twitter_test";
}
