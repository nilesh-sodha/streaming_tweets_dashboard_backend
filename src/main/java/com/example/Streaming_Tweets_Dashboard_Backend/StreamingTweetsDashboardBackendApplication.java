package com.example.streaming_tweets_dashboard_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nileshsodha
 */
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class StreamingTweetsDashboardBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamingTweetsDashboardBackendApplication.class, args);
	}

}
