package com.example.streaming_tweets_dashboard_backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author nileshsodha
 */
@Data
@AllArgsConstructor
public class TweetModel {
    String user;
    String text;
}
