package com.example.streaming_tweets_dashboard_backend.controller;

import com.example.streaming_tweets_dashboard_backend.Constants;
import com.example.streaming_tweets_dashboard_backend.kafka.KafkaProducer;
import com.example.streaming_tweets_dashboard_backend.model.TweetModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @author nileshsodha
 */
@Slf4j @RestController
@RequestMapping(value = "/twitter", produces = MediaType.APPLICATION_JSON_VALUE)
public class TwitterController {

    @Autowired
    KafkaProducer producer;

    //This is test api not include in final build
    @RequestMapping(value = "/publish-tweet", method = {RequestMethod.GET})
    public boolean crawl(@RequestParam("username") String username,
        @RequestParam("tweet") String tweet) {
        TweetModel model = new TweetModel(username, tweet);
        return producer.publish(Constants.KAFKA_PUBLISH_TWEET_TEST, model);
    }
}
